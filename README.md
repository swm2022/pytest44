# Tests

[![coverage report](https://gitlab.com/swm2022/pytest44/badges/master/coverage.svg)](https://swm2022.gitlab.io/pytest44/)
[![pipeline status](https://gitlab.com/swm2022/pytest44/badges/master/pipeline.svg)](https://gitlab.com/swm2022/pytest44/-/commits/master)


* [Code coverage](https://swm2022.gitlab.io/pytest44/)
* [Unittest report](https://swm2022.gitlab.io/pytest44/report.html)

